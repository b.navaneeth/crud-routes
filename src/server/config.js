const dotenv = require("dotenv");

dotenv.config({
    path: "../../env",
});

module.exports = {
    port: process.env.PORT,
    user: process.env.USER_NAME,
    database: process.env.DATABASE,
    host: process.env.HOST,
    password: process.env.PASSWORD,
};
const express = require("express");
const path = require("path");
const ejs = require("ejs");

const app = express();
const { port } = require("./config");
const comments = require("./routes/comments/comments");
const photos = require("./routes/photos/photos");
const todos = require("./routes/todos/todos");
const albums = require("./routes/albums/albums");
const users = require("./routes/users/users");
const posts = require("./routes/posts/posts");

app.use(express.static(`${__dirname}/../public`));
app.set("views", path.join(__dirname, "../public"));
app.set("view engine", "ejs");

comments()
    .then((data) => {
        app.use(data);
        return photos();
    })
    .then((data) => {
        app.use(data);
        return todos();
    })
    .then((data) => {
        app.use(data);
        return albums();
    })
    .then((data) => {
        app.use(data);
        return users();
    })
    .then((data) => {
        app.use(data);
        return posts();
    })
    .then((data) => {
        app.use(data);
    })
    .catch((err) => {
        console.log(err);
    });

app.use(function(err, req, res, next) {
    console.log(err);
    res.status(500);
    res.render("error");
});

app.listen(port);
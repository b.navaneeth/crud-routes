const express = require("express");
const bodyParser = require("body-parser");
const router = express.Router();
const deleteData = require("../../queries/todo/delete");

let urlencodedParser = bodyParser.urlencoded({ extended: false });

module.exports = function() {
    return new Promise((resolve, reject) => {
        try {
            router.get("/todos/delete", (req, res) => {
                res.status(200);
                res.render("deleteTodos");
            });

            router.post("/todos/delete", urlencodedParser, async(req, res, next) => {
                try {
                    if (await deleteData(req.body.id)) {
                        res.redirect("/todos");
                    } else {
                        res.render("deleteTodos", {
                            message: "Not Valid Id",
                        });
                    }
                } catch (err) {
                    next(err);
                }
            });
            resolve(router);
        } catch (err) {
            reject(err);
        }
    });
};
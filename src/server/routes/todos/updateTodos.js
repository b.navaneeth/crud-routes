const express = require("express");
const bodyParser = require("body-parser");
const router = express.Router();
const updateData = require("../../queries/todo/update");

let urlencodedParser = bodyParser.urlencoded({ extended: false });

module.exports = function() {
    return new Promise((resolve, reject) => {
        try {
            router.get("/todos/update", (req, res) => {
                res.status(200);
                res.render("updateTodos");
            });

            router.post("/todos/update", urlencodedParser, async(req, res, next) => {
                try {
                    if (await updateData(req.body)) {
                        res.redirect("/todos");
                    } else {
                        res.render("updateTodos", {
                            message: "Not valid ID",
                        });
                    }
                } catch (err) {
                    next(err);
                }
            });
            resolve(router);
        } catch (err) {
            reject(err);
        }
    });
};
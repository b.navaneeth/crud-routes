const express = require("express");
const routes = express.Router();

const show = require("./showTodos");
const add = require("./addTodos");
const update = require("./updateTodos");
const Delete = require("./deleteTodos");

module.exports = function() {
    return new Promise((resolve, reject) => {
        show()
            .then((data) => {
                routes.use(data);
                return add();
            })
            .then((data) => {
                routes.use(data);
                return update();
            })
            .then((data) => {
                routes.use(data);
                return Delete();
            })
            .then((data) => {
                routes.use(data);
                resolve(routes);
            })
            .catch((err) => {
                reject(err);
            });
    });
};
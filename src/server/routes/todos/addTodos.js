const express = require("express");
const bodyParser = require("body-parser");
const router = express.Router();
const addData = require("../../queries/todo/create");
const checkUser = require("../../queries/checkUser");

let urlencodedParser = bodyParser.urlencoded({ extended: false });

module.exports = function() {
    return new Promise((resolve, reject) => {
        try {
            router.get("/todos/create", (req, res) => {
                res.status(200);
                res.render("createTodos");
            });

            router.post("/todos/create", urlencodedParser, async(req, res, next) => {
                try {
                    if (await checkUser(req.body.userId)) {
                        await addData(req.body);
                        res.redirect("/todos");
                    } else {
                        res.render("createTodos", {
                            message: " Enter Valid UserId",
                        });
                    }
                } catch (err) {
                    next(err);
                }
            });
            resolve(router);
        } catch (err) {
            reject(err);
        }
    });
};
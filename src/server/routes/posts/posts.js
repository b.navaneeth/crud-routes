const express = require("express");
const routes = express.Router();

const show = require("./showData");
const add = require("./create");
const update = require("./update");
const Delete = require("./delete");

module.exports = function() {
    return new Promise((resolve, reject) => {
        show()
            .then((data) => {
                routes.use(data);
                return add();
            })
            .then((data) => {
                routes.use(data);
                return Delete();
            })
            .then((data) => {
                routes.use(data);
                return update();
            })
            .then((data) => {
                routes.use(data);
                resolve(routes);
            })
            .catch((err) => {
                reject(err);
            });
    });
};
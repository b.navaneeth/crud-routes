const express = require("express");
const bodyParser = require("body-parser");
const router = express.Router();
const deleteData = require("../../queries/posts/deleteData");

let urlencodedParser = bodyParser.urlencoded({ extended: false });

module.exports = function() {
    return new Promise((resolve, reject) => {
        try {
            router.get("/delete", (req, res) => {
                res.status(200);
                res.render("delete");
            });

            router.post("/delete", urlencodedParser, async(req, res, next) => {
                try {
                    if (await deleteData(req.body.id)) {
                        res.redirect("/");
                    } else {
                        res.render("delete", {
                            message: "Not Valid Id",
                        });
                    }
                } catch (err) {
                    next(err);
                }
            });
            resolve(router);
        } catch (err) {
            reject(err);
        }
    });
};
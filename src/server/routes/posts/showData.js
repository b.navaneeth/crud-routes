const express = require("express");
const router = express.Router();

const getData = require("../../queries/posts/getData");

module.exports = function() {
    return new Promise((resolve, reject) => {
        try {
            router.get("/", (req, res, next) => {
                getData()
                    .then((data) => {
                        res.status(200);
                        res.render("show", { data });
                    })
                    .catch((err) => {
                        next(err);
                    });
            });
            resolve(router);
        } catch (err) {
            reject(err);
        }
    });
};
const express = require("express");
const bodyParser = require("body-parser");
const router = express.Router();
const addData = require("../../queries/posts/addData");
const checkUser = require("../../queries/checkUser");

let urlencodedParser = bodyParser.urlencoded({ extended: false });

module.exports = function() {
    return new Promise((resolve, reject) => {
        try {
            router.get("/create", (req, res) => {
                res.status(200);
                res.render("create");
            });

            router.post("/create", urlencodedParser, async(req, res, next) => {
                try {
                    if (await checkUser(req.body.userID)) {
                        await addData(req.body);
                        res.redirect("/");
                    } else {
                        res.render("create", {
                            message: " Enter Valid UserId",
                        });
                    }
                } catch (err) {
                    next(err);
                }
            });
            resolve(router);
        } catch (err) {
            reject(err);
        }
    });
};
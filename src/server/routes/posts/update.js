const express = require("express");
const bodyParser = require("body-parser");
const router = express.Router();
const updateData = require("../../queries/posts/updateData");

let urlencodedParser = bodyParser.urlencoded({ extended: false });

module.exports = function() {
    return new Promise((resolve, reject) => {
        try {
            router.get("/update", (req, res) => {
                res.status(200);
                res.render("update");
            });

            router.post("/update", urlencodedParser, async(req, res, next) => {
                try {
                    if (await updateData(req.body)) {
                        res.redirect("/");
                    } else {
                        res.render("update", {
                            message: "Not valid ID",
                        });
                    }
                } catch (err) {
                    next(err);
                }
            });

            router.use((req, res) => {
                res.status(404);
                res.render("pagenotfound");
            });
            resolve(router);
        } catch (err) {
            reject(err);
        }
    });
};
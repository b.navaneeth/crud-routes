const express = require("express");
const bodyParser = require("body-parser");
const router = express.Router();
const updateData = require("../../queries/users/updateUser");

let urlencodedParser = bodyParser.urlencoded({ extended: false });

module.exports = function() {
    return new Promise((resolve, reject) => {
        try {
            router.get("/user/update", (req, res) => {
                res.status(200);
                res.render("updateUser");
            });

            router.post("/user/update", urlencodedParser, async(req, res, next) => {
                try {
                    if (await updateData(req.body)) {
                        res.redirect("/user");
                    } else {
                        res.render("updateTodos", {
                            message: "Not valid ID",
                        });
                    }
                } catch (err) {
                    next(err);
                }
            });
            resolve(router);
        } catch (err) {
            reject(err);
        }
    });
};
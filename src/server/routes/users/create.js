const express = require("express");
const bodyParser = require("body-parser");
const router = express.Router();
const addData = require("../../queries/users/addUsers");

let urlencodedParser = bodyParser.urlencoded({ extended: false });

module.exports = function() {
    return new Promise((resolve, reject) => {
        try {
            router.get("/user/create", (req, res) => {
                res.status(200);
                res.render("createUser");
            });

            router.post("/user/create", urlencodedParser, async(req, res, next) => {
                try {
                    await addData(req.body);
                    res.redirect("/user");
                } catch (err) {
                    next(err);
                }
            });
            resolve(router);
        } catch (err) {
            reject(err);
        }
    });
};
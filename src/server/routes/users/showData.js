const express = require("express");
const router = express.Router();

const getData = require("../../queries/users/getUsers");

module.exports = function() {
    return new Promise((resolve, reject) => {
        try {
            router.get("/user", (req, res, next) => {
                getData()
                    .then((data) => {
                        res.status(200);
                        res.render("showUsers", { data });
                    })
                    .catch((err) => {
                        next(err);
                    });
            });
            resolve(router);
        } catch (err) {
            reject(err);
        }
    });
};
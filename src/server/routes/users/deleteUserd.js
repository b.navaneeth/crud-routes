const express = require("express");
const bodyParser = require("body-parser");
const router = express.Router();
const deleteData = require("../../queries/users/delete");

let urlencodedParser = bodyParser.urlencoded({ extended: false });

module.exports = function() {
    return new Promise((resolve, reject) => {
        try {
            router.get("/user/delete", (req, res) => {
                res.status(200);
                res.render("deleteUser");
            });

            router.post("/user/delete", urlencodedParser, async(req, res, next) => {
                try {
                    if (await deleteData(req.body.id)) {
                        res.redirect("/user");
                    } else {
                        res.render("deleteUser", {
                            message: "Not Valid Id",
                        });
                    }
                } catch (err) {
                    next(err);
                }
            });
            resolve(router);
        } catch (err) {
            reject(err);
        }
    });
};
const express = require("express");
const routes = express.Router();

const showComments = require("./showComments");
const addComments = require("./addComments");
const updateComments = require("./updateComments");
const deleteComments = require("./deleteComments");

module.exports = function() {
    return new Promise((resolve, reject) => {
        showComments()
            .then((data) => {
                routes.use(data);
                return addComments();
            })
            .then((data) => {
                routes.use(data);
                return updateComments();
            })
            .then((data) => {
                routes.use(data);
                return deleteComments();
            })
            .then((data) => {
                routes.use(data);
                resolve(routes);
            })
            .catch((err) => {
                reject(err);
            });
    });
};
const express = require("express");
const bodyParser = require("body-parser");
const router = express.Router();
const deleteData = require("../../queries/comments/delete");

let urlencodedParser = bodyParser.urlencoded({ extended: false });

module.exports = function() {
    return new Promise((resolve, reject) => {
        try {
            router.get("/comments/delete", (req, res) => {
                res.status(200);
                res.render("deleteComments");
            });

            router.post(
                "/comments/delete",
                urlencodedParser,
                async(req, res, next) => {
                    try {
                        if (await deleteData(req.body.id)) {
                            res.redirect("/comments");
                        } else {
                            res.render("deleteComments", {
                                message: "Not Valid Id",
                            });
                        }
                    } catch (err) {
                        next(err);
                    }
                }
            );
            resolve(router);
        } catch (err) {
            reject(err);
        }
    });
};
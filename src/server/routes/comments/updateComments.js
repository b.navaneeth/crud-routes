const express = require("express");
const bodyParser = require("body-parser");
const router = express.Router();
const updateData = require("../../queries/comments/update");

let urlencodedParser = bodyParser.urlencoded({ extended: false });

module.exports = function() {
    return new Promise((resolve, reject) => {
        try {
            router.get("/comments/update", (req, res) => {
                res.status(200);
                res.render("updateComment");
            });

            router.post(
                "/comments/update",
                urlencodedParser,
                async(req, res, next) => {
                    try {
                        if (await updateData(req.body)) {
                            res.redirect("/comments");
                        } else {
                            res.render("updateComment", {
                                message: "Not valid ID",
                            });
                        }
                    } catch (err) {
                        next(err);
                    }
                }
            );
            resolve(router);
        } catch (err) {
            reject(err);
        }
    });
};
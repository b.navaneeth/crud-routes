const express = require("express");
const bodyParser = require("body-parser");
const router = express.Router();
const addData = require("../../queries/comments/create");
const checkPost = require("../../queries/checkPost");

let urlencodedParser = bodyParser.urlencoded({ extended: false });

module.exports = function() {
    return new Promise((resolve, reject) => {
        try {
            router.get("/comments/create", (req, res) => {
                res.status(200);
                res.render("createComments");
            });

            router.post(
                "/comments/create",
                urlencodedParser,
                async(req, res, next) => {
                    try {
                        if (await checkPost(req.bodypostId)) {
                            await addData(req.body);
                            res.redirect("/comments");
                        } else {
                            res.render("createComments", {
                                message: "Enter Valid post Id",
                            });
                        }
                    } catch (err) {
                        next(err);
                    }
                }
            );
            resolve(router);
        } catch (err) {
            reject(err);
        }
    });
};
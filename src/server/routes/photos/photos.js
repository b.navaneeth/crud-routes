const express = require("express");
const routes = express.Router();

const showPhotos = require("./showPhotos");
const addPhotos = require("./addPhotos");
const updatePhotos = require("./updatePhotos");
const deletePhotos = require("./deletePhotos");

module.exports = function() {
    return new Promise((resolve, reject) => {
        showPhotos()
            .then((data) => {
                routes.use(data);
                return addPhotos();
            })
            .then((data) => {
                routes.use(data);
                return updatePhotos();
            })
            .then((data) => {
                routes.use(data);
                return deletePhotos();
            })
            .then((data) => {
                routes.use(data);
                resolve(routes);
            })
            .catch((err) => {
                reject(err);
            });
    });
};
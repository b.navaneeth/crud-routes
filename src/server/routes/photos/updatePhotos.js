const express = require("express");
const bodyParser = require("body-parser");
const router = express.Router();
const updateData = require("../../queries/photos/update");

let urlencodedParser = bodyParser.urlencoded({ extended: false });

module.exports = function() {
    return new Promise((resolve, reject) => {
        try {
            router.get("/photos/update", (req, res) => {
                res.status(200);
                res.render("updatePhotos");
            });

            router.post(
                "/photos/update",
                urlencodedParser,
                async(req, res, next) => {
                    try {
                        if (await updateData(req.body)) {
                            res.redirect("/photos");
                        } else {
                            res.render("updatePhotos", {
                                message: "Not valid ID",
                            });
                        }
                    } catch (err) {
                        next(err);
                    }
                }
            );
            resolve(router);
        } catch (err) {
            reject(err);
        }
    });
};
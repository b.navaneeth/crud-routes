const express = require("express");
const router = express.Router();

const getData = require("../../queries/photos/getData");

module.exports = function() {
    return new Promise((resolve, reject) => {
        try {
            router.get("/photos", (req, res, next) => {
                getData()
                    .then((data) => {
                        res.status(200);
                        res.render("showPhotos", { data });
                    })
                    .catch((err) => {
                        next(err);
                    });
            });
            resolve(router);
        } catch (err) {
            reject(err);
        }
    });
};
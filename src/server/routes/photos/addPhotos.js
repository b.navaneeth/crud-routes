const express = require("express");
const bodyParser = require("body-parser");
const router = express.Router();
const addData = require("../../queries/photos/create");
const checkAlbum = require("../../queries/checkAlbum");

let urlencodedParser = bodyParser.urlencoded({ extended: false });

module.exports = function() {
    return new Promise((resolve, reject) => {
        try {
            router.get("/photos/create", (req, res) => {
                res.status(200);
                res.render("createPhotos");
            });

            router.post(
                "/photos/create",
                urlencodedParser,
                async(req, res, next) => {
                    try {
                        if (await checkAlbum(req.body.albumId)) {
                            await addData(req.body);
                            res.redirect("/photos");
                        } else {
                            res.render("createPhotos", {
                                message: "Enter valid Album Id",
                            });
                        }
                    } catch (err) {
                        next(err);
                    }
                }
            );
            resolve(router);
        } catch (err) {
            reject(err);
        }
    });
};
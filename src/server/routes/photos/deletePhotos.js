const express = require("express");
const bodyParser = require("body-parser");
const router = express.Router();
const deleteData = require("../../queries/photos/delete");

let urlencodedParser = bodyParser.urlencoded({ extended: false });

module.exports = function() {
    return new Promise((resolve, reject) => {
        try {
            router.get("/photos/delete", (req, res) => {
                res.status(200);
                res.render("deletePhotos");
            });

            router.post(
                "/photos/delete",
                urlencodedParser,
                async(req, res, next) => {
                    try {
                        if (await deleteData(req.body.id)) {
                            res.redirect("/photos");
                        } else {
                            res.render("deletePhotos", {
                                message: "Not Valid Id",
                            });
                        }
                    } catch (err) {
                        next(err);
                    }
                }
            );
            resolve(router);
        } catch (err) {
            reject(err);
        }
    });
};
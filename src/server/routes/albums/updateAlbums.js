const express = require("express");
const bodyParser = require("body-parser");
const router = express.Router();
const updateData = require("../../queries/albums/update");

let urlencodedParser = bodyParser.urlencoded({ extended: false });

module.exports = function() {
    return new Promise((resolve, reject) => {
        try {
            router.get("/albums/update", (req, res) => {
                res.status(200);
                res.render("updateAlbums");
            });

            router.post(
                "/albums/update",
                urlencodedParser,
                async(req, res, next) => {
                    try {
                        if (await updateData(req.body)) {
                            res.redirect("/albums");
                        } else {
                            res.render("updateAlbums", {
                                message: "Not valid ID",
                            });
                        }
                    } catch (err) {
                        next(err);
                    }
                }
            );
            resolve(router);
        } catch (err) {
            reject(err);
        }
    });
};
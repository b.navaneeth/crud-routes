const express = require("express");
const bodyParser = require("body-parser");
const router = express.Router();
const deleteData = require("../../queries/albums/delete");

let urlencodedParser = bodyParser.urlencoded({ extended: false });

module.exports = function() {
    return new Promise((resolve, reject) => {
        try {
            router.get("/albums/delete", (req, res) => {
                res.status(200);
                res.render("deleteAlbums");
            });

            router.post(
                "/albums/delete",
                urlencodedParser,
                async(req, res, next) => {
                    try {
                        if (await deleteData(req.body.id)) {
                            res.redirect("/albums");
                        } else {
                            res.render("deleteAlbums", {
                                message: "Not Valid Id",
                            });
                        }
                    } catch (err) {
                        next(err);
                    }
                }
            );
            resolve(router);
        } catch (err) {
            reject(err);
        }
    });
};
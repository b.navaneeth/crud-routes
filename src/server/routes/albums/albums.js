const express = require("express");
const routes = express.Router();

const show = require("./showAlbums");
const add = require("./addAlbums");
const update = require("./updateAlbums");
const Delete = require("./deleteAlbums");

module.exports = function() {
    return new Promise((resolve, reject) => {
        show()
            .then((data) => {
                routes.use(data);
                return add();
            })
            .then((data) => {
                routes.use(data);
                return update();
            })
            .then((data) => {
                routes.use(data);
                return Delete();
            })
            .then((data) => {
                routes.use(data);
                resolve(routes);
            })
            .catch((err) => {
                reject(err);
            });
    });
};
const express = require("express");
const bodyParser = require("body-parser");
const router = express.Router();
const addData = require("../../queries/albums/create");
const checkUser = require("../../queries/checkUser");

let urlencodedParser = bodyParser.urlencoded({ extended: false });

module.exports = function() {
    return new Promise((resolve, reject) => {
        try {
            router.get("/albums/create", (req, res) => {
                res.status(200);
                res.render("createAlbums");
            });

            router.post(
                "/albums/create",
                urlencodedParser,
                async(req, res, next) => {
                    try {
                        if (await checkUser(req.body.userID)) {
                            await addData(req.body);
                            res.redirect("/albums");
                        } else {
                            res.render("createAlbums", {
                                message: "Enter valid userId",
                            });
                        }
                    } catch (err) {
                        next(err);
                    }
                }
            );
            resolve(router);
        } catch (err) {
            reject(err);
        }
    });
};
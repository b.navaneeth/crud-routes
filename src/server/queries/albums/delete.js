const connect = require("../../connect");

module.exports = function(id) {
    return new Promise((resolve, reject) => {
        let query = "DELETE FROM albums WHERE id=?";
        let queryChild = "DELETE FROM photos WHERE albumId=?";
        connect.getConnection((err, connection) => {
            if (err) {
                reject(err);
            } else {
                connection.query(query, id, (err, result) => {
                    if (err) {
                        reject(err);
                    } else {
                        if (result.affectedRows == 0) {
                            //resolve(false);
                        } else {
                            //resolve(true);
                        }
                    }
                });
                connection.query(queryChild, id, (err, result) => {
                    if (err) {
                        reject(err);
                    }
                    resolve(true);
                    connection.release();
                });
            }
        });
    });
};
const Connect = require("../../connect.js");

module.exports = function(data) {
    return new Promise((resolve, reject) => {
        let query = `UPDATE albums SET ? WHERE id=?`;
        let values = {};
        if (data.checkboxTitle == "on") {
            values["title"] = data.title;
        }
        if (Object.keys(values).length === 0) {
            resolve(true);
        }
        Connect.getConnection((err, connection) => {
            if (err) {
                reject(err);
            } else {
                connection.query(query, [values, parseInt(data.id)], (err, result) => {
                    if (err) {
                        reject(err);
                    } else {
                        if (result.affectedRows == 0) {
                            resolve(false);
                        } else {
                            resolve(true);
                        }
                    }
                    connection.release();
                });
            }
        });
    });
};
const connect = require("../../connect");

module.exports = function(id) {
    return new Promise((resolve, reject) => {
        let query = "DELETE FROM users WHERE id=?";
        let queryChild = "DELETE FROM comments WHERE postId=?";
        connect.getConnection((err, connection) => {
            if (err) {
                reject(err);
            } else {
                connection.query(query, id, (err, result) => {
                    if (err) {
                        reject(err);
                    } else {
                        if (result.affectedRows == 0) {
                            //resolve(false);
                        } else {
                            //resolve(true);
                        }
                    }
                    //connection.release();
                });
                connection.query(queryChild, id, (err, result) => {
                    if (err) {
                        reject(err);
                    }
                    resolve(true);
                    connection.release();
                });
            }
        });
    });
};
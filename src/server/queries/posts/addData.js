const Connect = require("../../connect.js");

module.exports = function(data) {
    return new Promise((resolve, reject) => {
        let insert = `INSERT INTO users SET ?`;

        Connect.getConnection((err, connection) => {
            if (err) {
                reject(err);
            } else {
                connection.query(insert, data, (err, data) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve();
                    }
                    connection.release();
                });
            }
        });
    });
};
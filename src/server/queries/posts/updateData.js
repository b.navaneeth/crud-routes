const Connect = require("../../connect.js");

module.exports = function(data) {
    return new Promise((resolve, reject) => {
        let query;
        let values = [];
        if (data.checkboxTitel == "on" && data.checkboxBody == "on") {
            query = `UPDATE users SET title=? ,body=? WHERE id=?`;
            values.push(data.title);
            values.push(data.body);
            values.push(data.id);
        }
        if (typeof data.checkboxTitel == "undefined" && data.checkboxBody == "on") {
            query = `UPDATE users SET body=? WHERE id=?`;
            values.push(data.body);
            values.push(data.id);
        }
        if (data.checkboxTitel == "on" && typeof data.checkboxBody == "undefined") {
            query = `UPDATE users SET title=? WHERE id=?`;
            values.push(data.title);
            values.push(data.id);
        }
        if (
            typeof data.checkboxTitel == "undefined" &&
            typeof data.checkboxBody == "undefined"
        ) {
            resolve(true);
        }

        Connect.getConnection((err, connection) => {
            if (err) {
                reject(err);
            } else {
                connection.query(query, values, (err, result) => {
                    if (err) {
                        reject(err);
                    } else {
                        if (result.affectedRows == 0) {
                            resolve(false);
                        } else {
                            resolve(true);
                        }
                    }
                    connection.release();
                });
            }
        });
    });
};
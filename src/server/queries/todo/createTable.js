const connect = require("../../connect");
const fetch = require("node-fetch");

async function main() {
    let data = await fetch("https://jsonplaceholder.typicode.com/todos");
    data = await data.json();
    let InsertData = [];
    for (let element in data) {
        let arr = [];
        arr.push(data[element]["userId"]);
        arr.push(data[element]["id"]);
        arr.push(data[element]["title"]);
        arr.push(data[element]["completed"]);
        InsertData.push(arr);
    }
    let createQuery = `CREATE TABLE IF NOT EXISTS todos (
    userId INT,
    id INT PRIMARY KEY AUTO_INCREMENT,
    title TEXT,  
    completed BOOLEAN
)`;

    let insertQuery = "INSERT INTO todos VALUES ?";

    connect.getConnection((err, connection) => {
        if (err) {
            console.log(err);
        } else {
            connection.query(createQuery, (err, result, fields) => {
                if (err) {
                    console.log(err);
                } else {
                    console.log("Table created");
                }
                connection.release();
            });
            connection.query(insertQuery, [InsertData], (err, result) => {
                if (err) {
                    console.log(err);
                } else {
                    console.log("Data Insterted");
                }
                connect.end();
            });
        }
    });
}

main();
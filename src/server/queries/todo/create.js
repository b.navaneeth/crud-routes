const Connect = require("../../connect.js");

module.exports = function(data) {
    return new Promise((resolve, reject) => {
        let insert = `INSERT INTO todos SET ?`;
        let values = {
            userId: data.userId,
            title: data.title,
            completed: false,
        };
        Connect.getConnection((err, connection) => {
            if (err) {
                reject(err);
            } else {
                connection.query(insert, values, (err, data) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve();
                    }
                    connection.release();
                });
            }
        });
    });
};
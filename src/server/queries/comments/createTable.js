const connect = require("../../connect");
const fetch = require("node-fetch");

async function main() {
    let data = await fetch("https://jsonplaceholder.typicode.com/comments");
    data = await data.json();
    let InsertData = [];
    for (let element in data) {
        let arr = [];
        arr.push(data[element]["postId"]);
        arr.push(data[element]["id"]);
        arr.push(data[element]["name"]);
        arr.push(data[element]["email"]);
        arr.push(data[element]["body"]);
        InsertData.push(arr);
    }
    let createQuery = `CREATE TABLE IF NOT EXISTS comments (
    postId INT,
    id INT PRIMARY KEY AUTO_INCREMENT,
    name TEXT,  
    email TEXT,
    body TEXT 
)`;

    let insertQuery = "INSERT INTO comments VALUES ?";

    connect.getConnection((err, connection) => {
        if (err) {
            console.log(err);
        } else {
            connection.query(createQuery, (err, result, fields) => {
                if (err) {
                    console.log(err);
                } else {
                    console.log("Table created");
                }
                connection.release();
            });
            connection.query(insertQuery, [InsertData], (err, result) => {
                if (err) {
                    console.log(err);
                } else {
                    console.log("Data Insterted");
                }
                connect.end();
            });
        }
    });
}

main();
const Connect = require("../../connect.js");

module.exports = function(data) {
    return new Promise((resolve, reject) => {
        let query = `UPDATE comments SET ? WHERE id=?`;
        let values = {};
        if (data.checkboxName == "on") {
            values["name"] = data.name;
        }
        if (data.checkboxEmail == "on") {
            values["email"] = data.email;
        }
        if (data.checkboxBody == "on") {
            values["body"] = data.body;
        }
        if (Object.keys(values).length === 0) {
            resolve(true);
        }
        Connect.getConnection((err, connection) => {
            if (err) {
                reject(err);
            } else {
                connection.query(query, [values, data.id], (err, result) => {
                    if (err) {
                        reject(err);
                    } else {
                        if (result.affectedRows == 0) {
                            resolve(false);
                        } else {
                            resolve(true);
                        }
                    }
                    connection.release();
                });
            }
        });
    });
};
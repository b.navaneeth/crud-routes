const connect = require("../../connect");
const fetch = require("node-fetch");

async function main() {
    let data = await fetch("https://jsonplaceholder.typicode.com/users");
    data = await data.json();
    //console.log(data);

    let InsertData = [];
    for (let element in data) {
        let arr = [];
        arr.push(data[element]["id"]);
        arr.push(data[element]["name"]);
        arr.push(data[element]["username"]);
        arr.push(data[element]["email"]);
        arr.push(data[element]["address"]["street"]);
        arr.push(data[element]["address"]["suite"]);
        arr.push(data[element]["address"]["city"]);
        arr.push(data[element]["address"]["zipcode"]);
        arr.push(data[element]["address"]["geo"]["lat"]);
        arr.push(data[element]["address"]["geo"]["lng"]);
        //arr.push(data[element]["phone"]);
        arr.push(data[element]["website"]);
        arr.push(data[element]["company"]["name"]);
        arr.push(data[element]["company"]["catchPhrase"]);
        arr.push(data[element]["company"]["bs"]);
        InsertData.push(arr);
    }
    //console.log(InsertData[0]);

    let createQuery = `CREATE TABLE IF NOT EXISTS user_data (
        id INT PRIMARY KEY AUTO_INCREMENT,
        name TEXT,
        username VARCHAR(100) UNIQUE,
        email VARCHAR(100) UNIQUE,
        street TEXT,
        suite TEXT,
        city TEXT,
        zipcode TEXT,
        lat FLOAT( 10, 6 ),
        lng FLOAT( 10, 6 ),
        website TEXT,
        company_name TEXT,
        company_catchPhrase TEXT,
        company_bs TEXT
    )`;

    let insertQuery =
        "INSERT INTO user_data (id,name,username,email,street,suite,city,zipcode,lat,lng,website,company_name,company_catchPhrase,company_bs)VALUES ?";

    connect.getConnection((err, connection) => {
        if (err) {
            console.log(err);
        } else {
            connection.query(createQuery, (err, result, fields) => {
                if (err) {
                    console.log(err);
                } else {
                    console.log("Table created");
                }
                connection.release();
            });
            connection.query(insertQuery, [InsertData], (err, result) => {
                if (err) {
                    console.log(err);
                } else {
                    console.log("Data Insterted");
                }
                connect.end();
            });
        }
    });
}

main();
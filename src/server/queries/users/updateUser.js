const Connect = require("../../connect.js");

module.exports = function(data) {
    return new Promise((resolve, reject) => {
        let query = `UPDATE user_data SET ? WHERE id=?`;
        let values = {};
        if (data.checkboxName == "on") {
            values["name"] = data.name;
        }
        if (data.checkboxUsername == "on") {
            values["username"] = data.username;
        }
        if (data.checkboxEmail == "on") {
            values["email"] = data.email;
        }
        if (data.checkboxStreet == "on") {
            values["street"] = data.street;
        }
        if (data.checkboxSuite == "on") {
            values["suite"] = data.suite;
        }
        if (data.checkboxCity == "on") {
            values["city"] = data.city;
        }
        if (data.checkboxZip == "on") {
            values["zipcode"] = data.zipcode;
        }
        if (data.checkboxWebsite == "on") {
            values["website"] = data.website;
        }
        if (data.checkboxLat == "on") {
            values["lat"] = data.lat;
        }
        if (data.checkboxLng == "on") {
            values["lng"] = data.lng;
        }
        if (data.checkboxCompany_name == "on") {
            values["company_name"] = data.company_name;
        }
        if (data.checkboxCompany_catchPhrase == "on") {
            values["company_catchPhrase"] = data.company_catchPhrase;
        }
        if (data.checkBoxCompany_bs == "on") {
            values["company_bs"] = data.company_bs;
        }
        if (Object.keys(values).length === 0) {
            resolve(true);
        }
        Connect.getConnection((err, connection) => {
            if (err) {
                reject(err);
            } else {
                connection.query(query, [values, data.id], (err, result) => {
                    if (err) {
                        reject(err);
                    } else {
                        if (result.affectedRows == 0) {
                            resolve(false);
                        } else {
                            resolve(true);
                        }
                    }
                    connection.release();
                });
            }
        });
    });
};
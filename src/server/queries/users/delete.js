const connect = require("../../connect");
const deletePosts = require("../../queries/posts/deleteData");
const deleteAlbums = require("../../queries/albums/delete");
module.exports = function(id) {
    return new Promise((resolve, reject) => {
        let query = "DELETE FROM user_data WHERE id=?";
        let findPosts = "SELECT id FROM users WHERE userId=?";
        let findAlbums = "SELECT id FROM albums WHERE userId=?";
        connect.getConnection((err, connection) => {
            if (err) {
                reject(err);
            } else {
                connection.query(query, id, (err, result) => {
                    if (err) {
                        reject(err);
                    }
                });
                connection.query(findPosts, id, (err, result) => {
                    if (err) {
                        reject(err);
                    }
                    result.forEach(async(element) => {
                        console.log(element["id"]);
                        await deletePosts(element["id"]);
                    });
                });
                connection.query(findAlbums, id, (err, result) => {
                    if (err) {
                        reject(err);
                    }
                    result.forEach(async(element) => {
                        console.log(element["id"]);
                        await deleteAlbums(element["id"]);
                    });

                    resolve(true);
                    connection.release();
                });
            }
        });
    });
};
const connect = require("../../connect");

module.exports = function() {
    return new Promise((resolve, reject) => {
        let query = "SELECT * FROM photos";

        connect.getConnection((err, connection) => {
            if (err) {
                reject(err);
            } else {
                connection.query(query, (err, result) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(result);
                    }
                    connection.release();
                });
            }
        });
    });
};
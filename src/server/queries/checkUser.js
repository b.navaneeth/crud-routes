const Connect = require("../connect.js");

module.exports = function(id) {
    return new Promise((resolve, reject) => {
        let insert = `SELECT *  FROM user_data WHERE id=?`;

        Connect.getConnection((err, connection) => {
            if (err) {
                reject(err);
            } else {
                connection.query(insert, id, (err, result) => {
                    if (err) {
                        reject(err);
                    } else {
                        if (result.length == 0) {
                            resolve(false);
                        } else {
                            resolve(true);
                        }
                    }
                    connection.release();
                });
            }
        });
    });
};